# Symfony docker skeleton

## Quick start
1. `composer install`
2. `docker-compose up`
3. go to http://localhost:8080

## Etapes pour mettre l'authentification jwt
1. Installer le bundle `composer require "lexik/jwt-authentication-bundle"`
2. Générer la paire clé privée/clé publique
    * la clé privée `openssl genrsa -out config/jwt/private.pem -aes256 4096`
    * décrypter la clé privée `openssl rsa -in config/jwt/private.pem -out config/jwt/private2.pem`
    * supprimer la clé privée cryptée `rm config/jwt/private.pem`
    * renommer la clé privée décryptée `mv config/jwt/private2.pem config/jwt/private.pem`
    * générer la clé publique `openssl rsa -pubout -in config/jwt/private.pem -out config/jwt/public.pem`
    * rendre accessible les clefs à php `chmod 755 config/jwt -R`
3. Configurer le firewall du login dans le security.yaml (voir le fichier)
4. Configurer le firewall principal dans le security.yaml (voir le fichier)
5. Ajouter la route du login dans le route.yaml
6. Vérifier que ça marche en faisant une requête POST vers l'url du login au format json en indiquant `username` et `password` dans le body de la request (pour ce qui est de la configuration des users, on peut le faire comme on le souhaite, soit en BDD avec sa propre entité, soit en memory dans le security.yaml, rien de particulier)