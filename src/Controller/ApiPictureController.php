<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use App\Entity\Picture;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\HttpFoundation\Request;
use App\Service\FileService;

/**
 * @Route("/api/picture", name="api_picture")
 */
class ApiPictureController extends Controller
{
    private $serializer;
    
    public function __construct(SerializerInterface $serializer) {
        $this->serializer = $serializer;
    }

    /**
     * @Route("/", methods="GET")
     */
    public function all(){
        $repo = $this->getDoctrine()->getRepository(Picture::class);
        $pictures = $repo->findAll();
        $json = $this->serializer->serialize($pictures, "json");

        return JsonResponse::fromJsonString($json);
    }
    
    /**
     * @Route("/", methods="POST")
     */
    public function add(Request $req, FileService $fileService) {
        //On récupère le fichier dans la request
        $image = $req->files->get("url");
        //On récupère l'url absolue de notre application
        $absoluteUrl = $req->getScheme() . '://' . $req->getHttpHost() . $req->getBasePath();
        //On utilise le fileService pour uploader l'image
        $imageUrl = $fileService->upload($image, $absoluteUrl);

        $picture = new Picture();
        $picture->setDescription($req->get("description"));

        $picture->setDate(
            \DateTime::createFromFormat("d/m/Y", $req->get("date")));
        $picture->setUrl($imageUrl);

        $manager = $this->getDoctrine()->getManager();
        $manager->persist($picture);
        $manager->flush();

        $json = $this->serializer->serialize($picture, "json");

        return JsonResponse::fromJsonString($json);


    }

}
